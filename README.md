**Go version 1.21.3**

1. Add GOPRIVATE เพื่อ Import package Common Utils
 - [ ] Run command `go env -w GOPRIVATE=gitlab.com/dplus-dev/zettasoft/backend/microservice`

2. Set .gitconfig
 
- [ ] [url "https://gitlab.com/dplus-dev"]
    
	insteadOf = https://gitlab.com/dplus-dev
