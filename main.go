package main

import (
	"golang-boilerplate-template/src/api/v1/routes"
)

func main() {
	routes.StartServ()
}
