#=============================================================
#--------------------- build stage ---------------------------
#=============================================================
FROM golang:1.21.3-alpine AS build_stage
RUN apk update && apk add git
ENV PACKAGE_PATH=aumcontrolth/golangsaleplus
RUN mkdir -p /go/src/
WORKDIR /go/src/$PACKAGE_PATH

ENV PATH="/usr/local/go/bin:$PATH"
COPY . /go/src/$PACKAGE_PATH/
RUN git config --global url."https://$GIT_USER_PRIVATE:$GIT_TOKEN_PRIVTE@gitlab.com/aumcontrolth/golangsaleplus".insteadOf "https://gitlab.com/aumcontrolth/golangsaleplus"
RUN go env -w GONOSUMDB="gitlab.com/aumcontrolth/golangsaleplus"
RUN go env -w GOPRIVATE="gitlab.com/aumcontrolth/golangsaleplus"
RUN go clean -modcache
RUN go mod tidy
RUN go build -o golangsaleplus
#=============================================================
#--------------------- final stage ---------------------------
#=============================================================
FROM golang:1.21.3-alpine AS final_stage

ENV PACKAGE_PATH=aumcontrolth

ENV http_proxy=
ENV https_proxy=

COPY --from=build_stage /go/src/$PACKAGE_PATH/golangsaleplus /go/src/$PACKAGE_PATH/
COPY --from=build_stage /go/src/$PACKAGE_PATH/configs /go/src/$PACKAGE_PATH/configs

WORKDIR /go/src/$PACKAGE_PATH/

ENTRYPOINT ./golangsaleplus {ENV}
EXPOSE 3891
