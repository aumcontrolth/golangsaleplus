package auth

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

func (server *Server) RegisterRoutes(app *fiber.App, conf *utils.SystemConfig) {
	iMiddleware := utils.NewMiddleware()
	r := app.Group("/auth")
	r.Post("/login", server.Login)
	r.Get("/logout", iMiddleware.AuthorizationRequired(conf.Authentication.SecretKey), server.Logout)
}
