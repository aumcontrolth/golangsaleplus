package auth

import (
	"encoding/json"
	"golang-boilerplate-template/src/api/v1/models"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/logger"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

func (server *Server) Logout(c *fiber.Ctx) error {
	start := time.Now()
	response := utils.ResponseStandard{}
	getLogModelRequest := logger.LogModelRequest{
		ServiceType:   "Logout",
		CorrelationID: c.GetRespHeader("X-Request-Id"),
		Method:        "POST",
		StepName:      "Logout",
		Start:         start,
		Suffix:        server.Config.Log.Kibana.Suffix,
	}
	logModel := logger.GetLogModel(getLogModelRequest)

	defer func() {
		logOrderRequest := logger.LogOrderRequest{
			Response:   string(c.Response().Body()),
			ResultCode: response.Code,
			ResultDesc: response.Message,
		}
		if response.Code == "200" {
			logOrderRequest.Response = "Success"
		}
		logger.LogOrder(logOrderRequest, logModel, start)
	}()

	response, err := server.api.ServiceAuth.LogoutService(c.Get("Authorization"), logModel)

	if err != nil {
		return utils.ResponseFailed(c, fiber.StatusBadRequest, "failed", err)
	}

	if response.Code != "200" {
		return utils.ResponseFailed(c, fiber.StatusInternalServerError, response.Message, err)
	}

	return utils.ResponseSuccess(c, "success", response.Result)
}

func (server *Server) Login(c *fiber.Ctx) error {
	start := time.Now()
	var req models.LoginRequest
	if err := c.BodyParser(&req); err != nil {
		return utils.ResponseFailed(c, fiber.StatusBadRequest, "failed", err)

	}
	response := utils.ResponseStandard{}

	v := validator.New()
	err := v.Struct(req)
	if err != nil {
		return utils.ResponseFailed(c, fiber.StatusBadRequest, "Required Field", err)
	}

	getLogModelRequest := logger.LogModelRequest{
		ServiceType:   "Login",
		CorrelationID: c.GetRespHeader("X-Request-Id"),
		Method:        "POST",
		StepName:      "Login",
		Start:         start,
		Suffix:        server.Config.Log.Kibana.Suffix,
	}
	logModel := logger.GetLogModel(getLogModelRequest)

	defer func() {
		logReq := req
		bt, _ := json.Marshal(logReq)
		logOrderRequest := logger.LogOrderRequest{
			Request:    string(bt),
			Response:   string(c.Response().Body()),
			ResultCode: response.Code,
			ResultDesc: response.Message,
		}
		if response.Code == "200" {
			logOrderRequest.Response = "Success"
		}
		logger.LogOrder(logOrderRequest, logModel, start)
	}()

	response, err = server.api.ServiceAuth.LoginService(req, server.SystemConfig.Authentication.SecretKey, logModel)
	if response.Code != "200" {
		return utils.ResponseFailed(c, fiber.StatusInternalServerError, response.Message, err)
	}
	return utils.ResponseSuccess(c, "success", response.Result)
}
