package auth

import (
	authService "golang-boilerplate-template/src/api/v1/services/auth"

	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

type API struct {
	ServiceAuth authService.IAuthenService
}

type Server struct {
	api          API
	Config       utils.SystemConfigDatabaseList
	SystemConfig utils.SystemConfig
}

func NewServer(conf *utils.SystemConfig) *Server {

	confSystem, err := utils.ReadConfigDatabaseList("configs")
	systemDB := utils.SystemDatabase{
		MongoDB: confSystem.Database["mongodb"],
		Redis:   confSystem.Database["redis"],
		Mysql:   confSystem.Database["mysql"],
	}

	if err != nil {
		panic(err)
	}

	iAuthen := authService.NewIServiceAuth(authService.NewIServiceDaoAuth(systemDB))

	return &Server{
		SystemConfig: *conf,
		Config:       confSystem,
		api: API{
			ServiceAuth: iAuthen,
		},
	}
}
