package product

import (
	"encoding/json"
	"time"

	"golang-boilerplate-template/src/api/v1/models"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/logger"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

func (server *Server) GetProductByCriteria(c *fiber.Ctx) error {

	start := time.Now()

	var req models.ProductRequest
	if err := c.BodyParser(&req); err != nil {
		return utils.ResponseFailed(c, fiber.StatusBadRequest, "failed", err)

	}
	response := utils.ResponseStandard{}

	v := validator.New()
	err := v.Struct(req)
	if err != nil {
		return utils.ResponseFailed(c, fiber.StatusBadRequest, "Required Field", err)
	}

	getLogModelRequest := logger.LogModelRequest{
		ServiceType:   "GetProductByCriteria",
		CorrelationID: c.GetRespHeader("X-Request-Id"),
		Method:        "POST",
		StepName:      "GetProductByCriteria",
		Start:         start,
		Suffix:        server.Config.Log.Kibana.Suffix,
	}
	logModel := logger.GetLogModel(getLogModelRequest)

	defer func() {
		logReq := req
		bt, _ := json.Marshal(logReq)
		logOrderRequest := logger.LogOrderRequest{
			Request:    string(bt),
			Response:   string(c.Response().Body()),
			ResultCode: response.Code,
			ResultDesc: response.Message,
		}
		logger.LogOrder(logOrderRequest, logModel, start)
	}()

	reqByte, _ := json.Marshal(req)
	loggerModel := logger.LogModel{
		ServiceType: "GetProductByCriteria",
		Method:      "POST",
		StepName:    "GetProductByCriteria",
		Txid:        c.GetRespHeader("X-Request-Id"),
		Request:     string(reqByte),
		Suffix:      server.Config.Log.Kibana.Suffix,
	}
	response = server.api.ServiceProduct.QueryProductByCriteria(req, loggerModel)
	return utils.ResponseSuccess(c, "success", response.Result)
}
