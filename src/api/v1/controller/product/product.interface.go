package product

import (
	productService "golang-boilerplate-template/src/api/v1/services/product"

	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

type API struct {
	ServiceProduct productService.IProductService
}

type Server struct {
	api    API
	Config utils.SystemConfigDatabaseList
}

func NewServer() *Server {

	confSystem, err := utils.ReadConfigDatabaseList("configs")
	systemDB := utils.SystemDatabase{
		MongoDB: confSystem.Database["mongodb"],
		Redis:   confSystem.Database["redis"],
	}

	if err != nil {
		panic(err)
	}

	iProduct := productService.NewIServiceProduct(productService.NewIServiceDaoProduct(systemDB))

	return &Server{
		Config: confSystem,
		api: API{
			ServiceProduct: iProduct,
		},
	}
}
