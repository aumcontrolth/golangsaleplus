package product

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

func (server *Server) RegisterRoutes(app *fiber.App, conf *utils.SystemConfig) {
	iMiddleware := utils.NewMiddleware()
	r := app.Group(server.Config.Service.Endpoint + "/product")
	r.Post("/get-product-by-criteria", iMiddleware.AuthorizationRequired(conf.Authentication.SecretKey), server.GetProductByCriteria)
}
