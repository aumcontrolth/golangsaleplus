package visit

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

func (server *Server) RegisterRoutesVisit(app *fiber.App, conf *utils.SystemConfig) {
	r := app.Group(server.Config.Service.Endpoint + "/visit")
	r.Get("/suggestion-visit", server.GetSuggestionVisitController)
	r.Post("/get-best-seller-customer", server.GetBestSellerCustomerController)
	r.Post("/create-visit", server.CreateVisitController)

}
