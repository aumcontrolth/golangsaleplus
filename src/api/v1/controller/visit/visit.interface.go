package visit

import (
	visitService "golang-boilerplate-template/src/api/v1/services/visit"

	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

type API struct {
	ServiceVisit visitService.IVisitService
}

type Server struct {
	Config utils.SystemConfigDatabaseList
	api    API
}

func NewServer() *Server {
	confSystem, err := utils.ReadConfigDatabaseList("configs")
	systemDB := utils.SystemDatabase{
		Mysql: confSystem.Database["mysql"],
	}

	if err != nil {
		panic(err)
	}

	iVisit := visitService.NewIServiceVisit(visitService.NewIServiceDaoVisit(systemDB))

	return &Server{
		api: API{
			ServiceVisit: iVisit,
		},
		Config: confSystem,
	}
}
