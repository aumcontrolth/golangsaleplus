package visit

import (
	"golang-boilerplate-template/src/api/v1/models"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/logger"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

func (server *Server) GetSuggestionVisitController(c *fiber.Ctx) error {

	start := time.Now()

	var response utils.ResponseStandard

	getLogModelRequest := logger.LogModelRequest{
		ServiceType:   "GetSuggestionVisitController",
		CorrelationID: c.GetRespHeader("X-Request-Id"),
		Method:        "GET",
		StepName:      "GetSuggestionVisitController",
		Start:         start,
		Suffix:        server.Config.Log.Kibana.Suffix,
	}
	logModel := logger.GetLogModel(getLogModelRequest)

	defer func() {
		// logReq := req
		// bt, _ := json.Marshal(logReq)
		logOrderRequest := logger.LogOrderRequest{
			// Request:    string(bt),
			Response:   string(c.Response().Body()),
			ResultCode: response.Code,
			ResultDesc: response.Message,
		}
		logger.LogOrder(logOrderRequest, logModel, start)
	}()

	// reqByte, _ := json.Marshal(req)
	loggerModel := logger.LogModel{
		ServiceType: "GetSuggestionVisitController",
		Method:      "GET",
		StepName:    "GetSuggestionVisitController",
		Txid:        c.GetRespHeader("X-Request-Id"),
		// Request:     string(reqByte),
		Suffix: server.Config.Log.Kibana.Suffix,
	}
	response, err := server.api.ServiceVisit.GetSuggestionVisitService(loggerModel)
	if err != nil {
		return utils.ResponseFailed(c, 500, "failed", err)
	}

	return utils.ResponseSuccess(c, "success", response.Result)

}

func (server *Server) GetBestSellerCustomerController(c *fiber.Ctx) error {

	start := time.Now()

	var response utils.ResponseStandard

	var req models.GetBestSellerCustomerRequest

	if err := c.BodyParser(&req); err != nil {
		return utils.ResponseFailed(c, fiber.StatusBadRequest, "failed", err)
	}

	v := validator.New()
	err := v.Struct(req)
	if err != nil {
		return utils.ResponseFailed(c, fiber.StatusBadRequest, "Required Field", err)
	}

	getLogModelRequest := logger.LogModelRequest{
		ServiceType:   "GetBestSellerCustomerController",
		CorrelationID: c.GetRespHeader("X-Request-Id"),
		Method:        "GET",
		StepName:      "GetBestSellerCustomerController",
		Start:         start,
		Suffix:        server.Config.Log.Kibana.Suffix,
	}
	logModel := logger.GetLogModel(getLogModelRequest)

	defer func() {
		// logReq := req
		// bt, _ := json.Marshal(logReq)
		logOrderRequest := logger.LogOrderRequest{
			// Request:    string(bt),
			Response:   string(c.Response().Body()),
			ResultCode: response.Code,
			ResultDesc: response.Message,
		}
		logger.LogOrder(logOrderRequest, logModel, start)
	}()

	// reqByte, _ := json.Marshal(req)
	loggerModel := logger.LogModel{
		ServiceType: "GetBestSellerCustomerController",
		Method:      "GET",
		StepName:    "GetBestSellerCustomerController",
		Txid:        c.GetRespHeader("X-Request-Id"),
		// Request:     string(reqByte),
		Suffix: server.Config.Log.Kibana.Suffix,
	}
	response, err = server.api.ServiceVisit.GetBestSellerCustomerService(req, loggerModel)
	if err != nil {
		return utils.ResponseFailed(c, 500, "failed", err)
	}

	return utils.ResponseSuccess(c, "success", response.Result)

}

func (server *Server) CreateVisitController(c *fiber.Ctx) error {

	start := time.Now()

	var response utils.ResponseStandard

	var req models.CreateVisitRequest

	if err := c.BodyParser(&req); err != nil {
		return utils.ResponseFailed(c, fiber.StatusBadRequest, "failed", err)
	}

	v := validator.New()
	err := v.Struct(req)
	if err != nil {
		return utils.ResponseFailed(c, fiber.StatusBadRequest, "Required Field", err)
	}

	getLogModelRequest := logger.LogModelRequest{
		ServiceType:   "CreateVisitController",
		CorrelationID: c.GetRespHeader("X-Request-Id"),
		Method:        "GET",
		StepName:      "CreateVisitController",
		Start:         start,
		Suffix:        server.Config.Log.Kibana.Suffix,
	}
	logModel := logger.GetLogModel(getLogModelRequest)

	defer func() {
		// logReq := req
		// bt, _ := json.Marshal(logReq)
		logOrderRequest := logger.LogOrderRequest{
			// Request:    string(bt),
			Response:   string(c.Response().Body()),
			ResultCode: response.Code,
			ResultDesc: response.Message,
		}
		logger.LogOrder(logOrderRequest, logModel, start)
	}()

	// reqByte, _ := json.Marshal(req)
	loggerModel := logger.LogModel{
		ServiceType: "CreateVisitController",
		Method:      "GET",
		StepName:    "CreateVisitController",
		Txid:        c.GetRespHeader("X-Request-Id"),
		// Request:     string(reqByte),
		Suffix: server.Config.Log.Kibana.Suffix,
	}
	response, err = server.api.ServiceVisit.CreateVisitService(req, loggerModel)
	if err != nil {
		return utils.ResponseFailed(c, 500, "failed", err)
	}

	return utils.ResponseSuccess(c, "success", response.Result)

}
