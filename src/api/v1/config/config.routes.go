package config

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

func (server *Server) RegisterRoutes(app *fiber.App, conf *utils.SystemConfig) {
	app.Post(conf.Service.Endpoint+"/create-config", server.CreateConfig)
	app.Post(conf.Service.Endpoint+"/get-config", server.GetConfig)

}
