package config

import (
	"bytes"
	"encoding/json"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/logger"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/services/config"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

func (server *Server) GetConfig(c *fiber.Ctx) error {
	start := time.Now()
	req := config.GetConfigRequest{}
	if err := c.BodyParser(&req); err != nil {
		return utils.ResponseFailed(c, fiber.StatusBadRequest, "", err)
	}
	var response utils.ResponseStandard

	v := validator.New()
	err := v.Struct(req)
	if err != nil {
		return utils.ResponseFailed(c, fiber.StatusBadRequest, "Required Field", err)
	}
	trackingID := utils.GetUUID()
	getLogModelRequest := logger.LogModelRequest{
		ServiceType:   "GetConfig",
		Channel:       "Zetta-Utilities",
		CorrelationID: trackingID,
		Method:        "POST",
		StepName:      "GetConfig",
		Start:         start,
		TrackingID:    trackingID,
		Suffix:        server.Config.Log.Kibana.Suffix,
	}

	logModel := logger.GetLogModel(getLogModelRequest)

	defer func() {
		logOrderRequest := logger.LogOrderRequest{
			Request:    string(c.Body()),
			Response:   string(c.Response().Body()),
			ResultCode: response.Code,
			ResultDesc: response.Message,
		}
		logger.LogOrder(logOrderRequest, logModel, start)
	}()

	dataResponse, err := config.QueryCommonConfig(&logModel, req, server.Config.Database.MongoDB.Uri, server.Config.Database.MongoDB.DbName, server.Config.Database.Redis.Uri, "zetta_lov_mst")
	if err != nil {
		return utils.ResponseFailed(c, fiber.StatusInternalServerError, "", err)
	}
	if dataResponse != nil {
		data, _ := json.Marshal(dataResponse)
		response.Result = data
		response.Code = "200"
		response.Message = "SUCCESS"
	}

	return utils.ResponseSuccess(c, "success", response.Result)
}

func (server *Server) CreateConfig(c *fiber.Ctx) error {
	start := time.Now()
	req := config.CreateConfigRequest{}
	if err := c.BodyParser(&req); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON("")
	}

	var response utils.ResponseStandard

	trackingID := utils.GetUUID()
	getLogModelRequest := logger.LogModelRequest{
		ServiceType:   "CreateConfig",
		Channel:       "Zetta-Utilities",
		CorrelationID: trackingID,
		Method:        "POST",
		StepName:      "CreateConfig",
		Start:         start,
		TrackingID:    trackingID,
		Suffix:        server.Config.Log.Kibana.Suffix,
	}

	logModel := logger.GetLogModel(getLogModelRequest)

	defer func() {
		logOrderRequest := logger.LogOrderRequest{
			Request:    string(c.Body()),
			Response:   string(c.Response().Body()),
			ResultCode: response.Code,
			ResultDesc: response.Message,
		}
		logger.LogOrder(logOrderRequest, logModel, start)
	}()
	v := validator.New()
	err := v.Struct(req)
	if err != nil {
		return utils.ResponseFailed(c, fiber.StatusBadRequest, "Required Field", err)
	}

	createValue := config.CommonConfigStruct{}
	createByte, err := json.Marshal(req)
	if err != nil {
		return utils.ResponseFailed(c, fiber.StatusBadRequest, "", err)
	}

	//Not allow unknownfields from CommonConfigStruct{}
	dec := json.NewDecoder(bytes.NewReader(createByte))
	dec.DisallowUnknownFields()
	err = dec.Decode(&createValue)

	err = config.InsertCommonConfig(&logModel, createValue, server.Config.Database.MongoDB.Uri, server.Config.Database.MongoDB.DbName, server.Config.Database.Redis.Uri, "zetta_lov_mst")
	if err != nil {
		return utils.ResponseFailed(c, fiber.StatusInternalServerError, "", err)
	}

	return utils.ResponseSuccess(c, "success", nil)
}
