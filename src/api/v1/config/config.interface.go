package config

import (
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

type API struct {
	// Service createendpoint.IServiceCreateEndpoint
}

type Store struct {
	// IFlowConfig flowconfig.IFlowConfig
}

type Server struct {
	Config utils.SystemConfig
}

func NewServer() *Server {
	confSystem, err := utils.ReadConfig("configs")
	if err != nil {
		panic(err)
	}
	return &Server{
		Config: confSystem,
	}
}
