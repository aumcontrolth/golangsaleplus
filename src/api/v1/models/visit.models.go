package models

type GetBestSellerCustomerRequest struct {
	CustomerCode string `json:"customerCode" validate:"required"`
}

type DataTableVisitImage struct {
	Image      string `json:"image,omitempty"`
	MarketType string `json:"marketType" validate:"required,oneof=BEFORE-SHELF AFTER-SHELF SPECIAL-AREA SPECIAL-AREA-ADD PRODUCT-MISSING DOCUMENT-SORT"`
	ImageKey   string `json:"imageKey,omitempty"`
	No         int    `json:"no,omitempty"`
}

type DataVisitShelf struct {
	ShelfHeadId  string  `json:"shelfHeadId,omitempty"`
	ShelfLineId  string  `json:"shelfLineId,omitempty"`
	SubShelfName string  `json:"subShelfName,omitempty"`
	ValueStock   float64 `json:"valueStock,omitempty"`
	ValueOrder   float64 `json:"valueOrder,omitempty"`
}

type CreateVisitRequest struct {
	PreCode          string                `json:"preCode" validate:"required"`
	CustomerCode     string                `json:"customerCode,omitempty"`
	BranchId         string                `json:"branchId" validate:"required"`
	Lat              float64               `json:"lat" validate:"required"`
	Lng              float64               `json:"lng" validate:"required"`
	VciId            string                `json:"vciId" validate:"required"`
	TimeStartVisit   string                `json:"timeStartVisit" validate:"required"`
	TotalSKUMissing  int                   `json:"totalSKUMissing,omitempty"`
	BranchSKUMissing int                   `json:"branchSKUMissing,omitempty"`
	VisitImage       []DataTableVisitImage `json:"visitImage,omitempty"`
	VisitShelf       []DataVisitShelf      `json:"visitShelf,omitempty"`
}
