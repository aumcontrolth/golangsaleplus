package models

import "time"

type ProductRequest struct {
	ProductName string `json:"productName" validate:"required"`
	ProductCode string `json:"productCode"`
	CreateBy    string `json:"createBy,omitempty"`
}

type ProductBean struct {
	ProductName string    `json:"productName" bson:"productName" validate:"required"`
	ProductCode string    `json:"productCode" bson:"productCode" validate:"required"`
	SalePrice   uint32    `json:"salePrice" bson:"salePrice" validate:"required"`
	LaunchDate  time.Time `json:"launchDate" bson:"launchDate" validate:"required"`
	ExpireDate  time.Time `json:"expireDate" bson:"expireDate" validate:"required"`
	CreateBy    string    `json:"createBy" bson:"createBy" validate:"required"`
	CreateDate  time.Time `json:"createDate" bson:"createDate" `
	UpdateBy    string    `json:"updateBy" bson:"updateBy" `
	UpdateDate  time.Time `json:"updateDate" bson:"updateDate" `
	Remark      string    `json:"remark,omitempty" bson:"remark,omitempty"`
}

type ProductResponse struct {
	ProductList []ProductBean `json:"productList,omitempty"`
}
