package routes

import (
	"fmt"
	"golang-boilerplate-template/src/api/v1/auth"
	"golang-boilerplate-template/src/api/v1/config"
	"golang-boilerplate-template/src/api/v1/controller/product"
	"golang-boilerplate-template/src/api/v1/controller/visit"
	"log"
	"os"

	fiber "github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/monitor"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/database"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

func StartServ() {
	fmt.Println("--- StartServ ---")
	iMiddleware := utils.NewMiddleware()

	app := fiber.New()
	app.Use(recover.New(
		recover.Config{
			EnableStackTrace: true,
		},
	))
	app.Use(cors.New())
	app.Use(iMiddleware.InitialLimiter(20))
	app.Use(iMiddleware.InitialMiddlewareRequestId("X-Request-ID"))

	systemConfig, err := utils.ReadConfig("configs")
	if err != nil {
		fmt.Println("read config fail")
	}

	app.Get("/dashboard", monitor.New())

	app.Get("/health", func(c *fiber.Ctx) error {
		var check utils.Build
		if systemConfig.Build == check {
			return c.Status(fiber.StatusInternalServerError).JSON(nil)
		} else {
			type respCheckModel struct {
				Endpoint string `json:"endpoint"`
				AppName  string `json:"appName"`
				utils.Build
			}
			resp := respCheckModel{
				Endpoint: "/health",
				AppName:  systemConfig.Service.Name,
				Build:    systemConfig.Build,
			}
			_, err := database.GetMongoDB(systemConfig.Database.MongoDB.Uri, systemConfig.Database.MongoDB.DbName, nil)
			if err != nil {
				var env string
				if len(os.Args) > 0 {
					env = os.Args[1]
				}
				fmt.Println("Mongo Error ", env)
				return c.Status(fiber.StatusInternalServerError).JSON(nil)
			}

			return c.Status(fiber.StatusOK).JSON(resp)
		}
	})

	// err = database.InitRedis(systemConfig.Database.Redis.Uri, nil)
	// if err != nil {
	// 	panic(err.Error())
	// }

	// err = database.InitMongoDB(systemConfig.Database.MongoDB.Uri, systemConfig.Database.MongoDB.DbName, nil)
	// if err != nil {
	// 	panic(err.Error())
	// }

	err = database.InitialSQLServerDatabase(systemConfig.Database.Mysql.Uri)
	if err != nil {
		panic(err.Error())
	}

	// logger.InitialElasticLog(systemConfig.ElasticConfig.Address, systemConfig.ElasticConfig.Username, systemConfig.ElasticConfig.Password, systemConfig.ElasticConfig.CerticateFingerPrint, systemConfig.Log.Kibana.Suffix, systemConfig.Service.Domain)

	auth := auth.NewServer(&systemConfig)
	auth.RegisterRoutes(app, &systemConfig)

	co := config.NewServer()
	co.RegisterRoutes(app, &systemConfig)

	p := product.NewServer()
	p.RegisterRoutes(app, &systemConfig)

	visit := visit.NewServer()
	visit.RegisterRoutesVisit(app, &systemConfig)

	//Show Route
	for _, routes := range app.Stack() {
		for _, route := range routes {
			if route.Method == fiber.MethodGet || route.Method == fiber.MethodPost {
				fmt.Println(route.Method + ":" + route.Path)
			}
		}
	}

	log.Fatal(app.Listen(":" + systemConfig.Service.Port))

}
