package productService

import (
	"context"
	"encoding/json"
	"fmt"
	"golang-boilerplate-template/src/api/v1/models"
	"strings"
	"time"

	"github.com/go-redis/redis"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/database"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/logger"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type IServiceDaoProduct interface {
	QueryProductByCriteria(models.ProductRequest, logger.LogModel) (response models.ProductResponse, code string, err error)
}

type serviceDaoProduct struct {
	confDB utils.SystemDatabase
}

func NewIServiceDaoProduct(confDB utils.SystemDatabase) IServiceDaoProduct {
	return &serviceDaoProduct{
		confDB: confDB,
	}
}

func (r *serviceDaoProduct) QueryProductByCriteria(queryParam models.ProductRequest, log logger.LogModel) (response models.ProductResponse, code string, errResp error) {
	startStep := time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()

	reqByte, err := json.Marshal(queryParam)
	logStepRequest := logger.LogStepRequest{
		StepName:    "DAO- QueryProductByCriteria",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: string(reqByte),
		Endpoint:    "ZETTA",
		Method:      "DAO - QueryProductByCriteria",
		System:      "ZETTA",
	}

	redisTimeout := time.Duration(1) * time.Hour
	var out []byte
	redisKey := "QueryProductByCriteria_" + queryParam.ProductName + "_" + queryParam.ProductCode
	rdb, errRedisConnection := database.GetRedis(r.confDB.Redis.Uri, nil)
	if errRedisConnection == nil {
		redisKey = strings.ToLower(redisKey)
		matchKeys, errRedis := rdb.Get(ctx, redisKey).Result()
		if errRedis == nil && errRedis != redis.Nil {
			json.Unmarshal([]byte(matchKeys), &response)
			return
		}
	}
	client, err := database.GetMongoDB(r.confDB.MongoDB.Uri, r.confDB.MongoDB.DbName, nil)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}

	filterList := []bson.D{}
	if queryParam.ProductName != "" {
		filterList = append(filterList, bson.D{{Key: "productName", Value: bson.M{"$regex": queryParam.ProductName, "$options": "im"}}})
	}
	if queryParam.ProductCode != "" {
		filterList = append(filterList, bson.D{{Key: "productCode", Value: queryParam.ProductCode}})
	}
	if queryParam.ProductCode != "" {
		filterList = append(filterList, bson.D{{Key: "createBy", Value: queryParam.CreateBy}})
	}
	pipeline := []bson.M{
		{"$match": bson.M{"$and": filterList}},
		{
			"$project": bson.M{
				"_id": 0,
			},
		},
	}
	collections := client.Collection("products_mst")
	findOptions := options.Aggregate()
	cursor, err := collections.Aggregate(ctx, pipeline, findOptions)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMongoDB"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}

	var res []models.ProductBean
	for cursor.Next(ctx) {
		var doc models.ProductBean
		if err = cursor.Decode(&doc); err != nil {
			logStepRequest.ResultDesc = "Error : GetMongoDB"
			logStepRequest.Response = err.Error()
			logStepRequest.ResultCode = "500"
			logger.LogStep(logStepRequest, log, startStep)
			return
		}
		res = append(res, doc)
	}
	cursor.Close(ctx)

	// if len(res) > 0 {
	// 	response.ProductList = res
	// }

	if errRedisConnection == nil && len(res) != 0 {
		response.ProductList = res
		out, err = json.Marshal(response)
		if err != nil {
			fmt.Println("Cannot marshal redis key :: ", redisKey, " err :: ", err.Error())
		} else {
			RedisValue := string(out)
			redisKey = strings.ToLower(redisKey)
			err = rdb.Set(ctx, redisKey, RedisValue, redisTimeout).Err()
			if err != nil {
				fmt.Println("Cannot set redis key :: ", redisKey, " err :: ", err.Error())
			}
		}
	}

	logStepRequest.ResultCode = "200"
	logStepRequest.ResultDesc = "Success"
	logStepRequest.Response = "Success"

	logger.LogStep(logStepRequest, log, startStep)
	return
}
