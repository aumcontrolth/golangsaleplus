package productService

import (
	"golang-boilerplate-template/src/api/v1/models"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/logger"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

type IProductService interface {
	QueryProductByCriteria(models.ProductRequest, logger.LogModel) (response utils.ResponseStandard)
}

type serviceProduct struct {
	dao IServiceDaoProduct
}

func NewIServiceProduct(dao IServiceDaoProduct) IProductService {
	return &serviceProduct{
		dao: dao,
	}
}

func (r *serviceProduct) QueryProductByCriteria(req models.ProductRequest, logModel logger.LogModel) (response utils.ResponseStandard) {
	data, _, err := r.dao.QueryProductByCriteria(req, logModel)
	if err != nil {
		response.Code = "500"
		response.Message = err.Error()
		return
	}

	productList := data.ProductList
	if len(productList) == 0 {
		response.Code = "400"
		response.Message = "data not found"
		return
	}

	response.Code = "200"
	response.Result = fiber.Map{"productList": productList}
	response.Message = "Success"
	return
}
