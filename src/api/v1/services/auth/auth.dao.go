package authService

import (
	"context"
	"database/sql"
	"encoding/json"
	"time"

	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/database"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/logger"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

type IServiceDaoAuth interface {
	UpsertAuthentication(UserAuthenBean, logger.LogModel) (err error)
	LogoutAuthen(string, logger.LogModel) (err error)
	CheckAccessTokenAuthen(string, logger.LogModel) (err error)
}

type serviceDaoAuth struct {
	confDB utils.SystemDatabase
}

func NewIServiceDaoAuth(confDB utils.SystemDatabase) IServiceDaoAuth {
	return &serviceDaoAuth{
		confDB: confDB,
	}
}

func (r *serviceDaoAuth) UpsertAuthentication(req UserAuthenBean, log logger.LogModel) (err error) {
	startStep := time.Now()
	_, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()

	reqByte, err := json.Marshal(req)
	logStepRequest := logger.LogStepRequest{
		StepName:    "DAO- UpsertAuthentication",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: string(reqByte),
		Endpoint:    "ZETTA",
		Method:      "DAO - UpsertAuthentication",
		System:      "ZETTA",
	}

	client, err := database.GetMsClient(r.confDB.Mysql.Uri)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMsClient"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}

	stmt, err := client.Prepare(`EXEC DDMS_Authen_UpsertLogin @dplusId=@p1 , @deviceId=@p2 , @accessToken=@p3`)
	if err != nil {
		logStepRequest.ResultDesc = "Error : Prepare Statement DDMS_Authen_UpsertLogin"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}

	_, err = stmt.Exec(sql.Named("p1", req.DplusId), sql.Named("p2", req.DeviceId), sql.Named("p3", req.AccessToken))
	if err != nil {
		logStepRequest.ResultDesc = "Error : Exec DDMS_Authen_UpsertLogin"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}
	defer stmt.Close()

	return
}

func (r *serviceDaoAuth) LogoutAuthen(token string, log logger.LogModel) (err error) {
	startStep := time.Now()
	_, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()

	logStepRequest := logger.LogStepRequest{
		StepName:  "DAO- LogoutAuthen",
		StartDate: utils.ConvDatetimeFormatLog(startStep),
		Endpoint:  "ZETTA",
		Method:    "DAO - LogoutAuthen",
		System:    "ZETTA",
	}

	client, err := database.GetMsClient(r.confDB.Mysql.Uri)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMsClient"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}

	stmt, err := client.Prepare(`EXEC DDMS_Authen_Logout @accessToken=@p1`)
	if err != nil {
		logStepRequest.ResultDesc = "Error : Prepare Statement DDMS_Authen_Logout"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}

	_, err = stmt.Exec(sql.Named("p1", token))
	if err != nil {
		logStepRequest.ResultDesc = "Error : Exec DDMS_Authen_Logout"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}
	defer stmt.Close()

	return
}

func (r *serviceDaoAuth) CheckAccessTokenAuthen(token string, log logger.LogModel) (err error) {
	startStep := time.Now()
	_, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()

	logStepRequest := logger.LogStepRequest{
		StepName:  "DAO- CheckAccessTokenAuthen",
		StartDate: utils.ConvDatetimeFormatLog(startStep),
		Endpoint:  "ZETTA",
		Method:    "DAO - CheckAccessTokenAuthen",
		System:    "ZETTA",
	}

	client, err := database.GetMsClient(r.confDB.Mysql.Uri)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMsClient"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}

	stmt, err := client.Prepare(`EXEC DDMS_Authen_CheckAccessToken @accessToken=@p1`)
	if err != nil {
		logStepRequest.ResultDesc = "Error : Prepare Statement DDMS_Authen_CheckAccessToken"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}

	_, err = stmt.Exec(sql.Named("p1", token))
	if err != nil {
		logStepRequest.ResultDesc = "Error : Exec DDMS_Authen_CheckAccessToken"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}
	defer stmt.Close()

	return
}
