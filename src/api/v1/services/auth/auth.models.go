package authService

type UserAuthenBean struct {
	DplusId     string `json:"dplusId"`
	DeviceId    string `json:"deviceId"`
	AccessToken string `json:"accessToken"`
}
