package authService

import (
	"golang-boilerplate-template/src/api/v1/models"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/logger"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

var iMiddleware = utils.NewMiddleware()

type IAuthenService interface {
	LoginService(req models.LoginRequest, secret string, logModel logger.LogModel) (response utils.ResponseStandard, err error)
	CheckAccessTokenService(token string, logModel logger.LogModel) (response utils.ResponseStandard, err error)
	LogoutService(token string, logModel logger.LogModel) (response utils.ResponseStandard, err error)
}

type serviceAuth struct {
	dao IServiceDaoAuth
}

func NewIServiceAuth(dao IServiceDaoAuth) IAuthenService {
	return &serviceAuth{
		dao: dao,
	}
}

func (r *serviceAuth) LoginService(req models.LoginRequest, secret string, logModel logger.LogModel) (response utils.ResponseStandard, err error) {
	// to do something bussiness logic

	//

	var data map[string]interface{}
	data = make(map[string]interface{})

	data["dplusId"] = req.Username
	data["loginDate"] = time.Now()

	token, err := iMiddleware.CreateToken(data, secret, 24)
	if err != nil {
		response.Code = "500"
		response.Message = err.Error()
		return
	}

	bean := UserAuthenBean{
		DplusId:     "ZT00000",
		DeviceId:    utils.GetUUID(),
		AccessToken: token,
	}

	err = r.dao.UpsertAuthentication(bean, logModel)

	if err != nil {
		response.Code = "500"
		response.Message = err.Error()
		return
	}

	response.Code = "200"
	response.Result = fiber.Map{"token": token}
	response.Message = "Success"

	return
}

func (r *serviceAuth) CheckAccessTokenService(token string, logModel logger.LogModel) (response utils.ResponseStandard, err error) {
	// to do something bussiness logic

	//
	return
}

func (r *serviceAuth) LogoutService(token string, logModel logger.LogModel) (response utils.ResponseStandard, err error) {
	// to do something bussiness logic

	//

	//token = Bearer xxx
	parts := strings.Split(token, " ")
	err = r.dao.LogoutAuthen(parts[1], logModel)
	if err != nil {
		response.Code = "500"
		response.Message = err.Error()
		return
	}
	response.Code = "200"
	response.Result = nil
	response.Message = "Success"
	return
}
