package visitService

import (
	"fmt"
	"golang-boilerplate-template/src/api/v1/models"
	"strings"

	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/logger"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

type IVisitService interface {
	GetSuggestionVisitService(log logger.LogModel) (utils.ResponseStandard, error)
	GetBestSellerCustomerService(req models.GetBestSellerCustomerRequest, log logger.LogModel) (utils.ResponseStandard, error)
	CreateVisitService(req models.CreateVisitRequest, log logger.LogModel) (utils.ResponseStandard, error)
}

type serviceVisit struct {
	dao IServiceDaoVisit
}

func NewIServiceVisit(dao IServiceDaoVisit) IVisitService {
	return &serviceVisit{
		dao: dao,
	}
}

func convertToMapImageVisit(arrImage []models.DataTableVisitImage) []map[string]interface{} {
	var result []map[string]interface{}
	for _, item := range arrImage {
		result = append(result, map[string]interface{}{
			"image":      item.Image,
			"marketType": item.MarketType,
			"no":         item.No,
		})
	}
	return result
}

func onConvertImageVisit(arrImage []map[string]interface{}) string {
	if len(arrImage) <= 0 {
		return "DECLARE @temp2 DT23D_ImageVisitLineTable"
	}

	var imageUrl strings.Builder
	for i, e := range arrImage {
		image := e["image"].(string)
		marketType := e["marketType"].(string)
		no := e["no"].(int)
		imageUrl.WriteString(fmt.Sprintf("(%d, '%s', '%s', %d),", i, image, marketType, no))
	}
	imageUrlStr := strings.TrimSuffix(imageUrl.String(), ",")
	temp2ImageVisit := fmt.Sprintf("DECLARE @temp2 DT23D_ImageVisitLineTable\nINSERT INTO @temp2 (indx, image, marketType, no) VALUES %s", imageUrlStr)

	return temp2ImageVisit
}

func convertToMapShelfVisit(arrImage []models.DataVisitShelf) []map[string]interface{} {
	var result []map[string]interface{}
	for _, item := range arrImage {
		result = append(result, map[string]interface{}{
			"shelfHeadId":  item.ShelfHeadId,
			"shelfLineId":  item.ShelfLineId,
			"subShelfName": item.SubShelfName,
			"valueStock":   item.ValueStock,
			"valueOrder":   item.ValueOrder,
		})
	}
	return result
}

func onConvertShelfVisit(arrImage []map[string]interface{}) string {
	if len(arrImage) <= 0 {
		return "DECLARE @temp3 DT23D_ShelfVisitLineTable"
	}

	var imageUrl strings.Builder
	for i, e := range arrImage {
		shelfHeadId := e["shelfHeadId"].(string)
		shelfLineId := e["shelfLineId"].(string)
		subShelfName := e["subShelfName"].(string)
		valueStock := e["valueStock"].(float64)
		valueOrder := e["valueOrder"].(float64)
		imageUrl.WriteString(fmt.Sprintf("(%d, '%s', '%s', '%s', %f, %f),", i, shelfHeadId, shelfLineId, subShelfName, valueStock, valueOrder))
	}
	imageUrlStr := strings.TrimSuffix(imageUrl.String(), ",")
	temp3ShelfVisit := fmt.Sprintf("DECLARE @temp3 DT23D_ShelfVisitLineTable\nINSERT INTO @temp3 (indx, shelfHeadId, shelfLineId, subShelfName, valueStock, valueOrder) VALUES %s", imageUrlStr)
	return temp3ShelfVisit
}

func (r *serviceVisit) GetSuggestionVisitService(log logger.LogModel) (res utils.ResponseStandard, err error) {
	resp, err := r.dao.GetSuggestionVisit(log)

	if err != nil {
		res.Result = nil
		res.Code = "500"
		res.ErrorMessage = err.Error()
		return
	}
	res.Code = "200"
	res.Result = resp

	return
}

func (r *serviceVisit) GetBestSellerCustomerService(req models.GetBestSellerCustomerRequest, log logger.LogModel) (response utils.ResponseStandard, err error) {
	resp, err := r.dao.GetBestSellerCustomer(req, log)
	response.Result = resp
	return
}

func (r *serviceVisit) CreateVisitService(req models.CreateVisitRequest, log logger.LogModel) (response utils.ResponseStandard, err error) {
	bindingData := fmt.Sprintf("'%s', '%s', '%s', '%s', '%s', '%s', '%f', '%f', '%s', '%s', '%s', '%d', '%d', '%s'",
		"DP65084",
		"ADSP-001",
		"paitpong.toi@dplusonline.net",
		req.PreCode,
		req.CustomerCode,
		req.BranchId,
		req.Lat,
		req.Lng,
		req.VciId,
		req.TimeStartVisit,
		"2023-02-03 16:59:46.243",
		req.TotalSKUMissing,
		req.BranchSKUMissing,
		"DPL",
	)
	arrImageTemp2 := convertToMapImageVisit(req.VisitImage)
	temp2 := onConvertImageVisit(arrImageTemp2)
	arrImageTemp3 := convertToMapShelfVisit(req.VisitShelf)
	temp3 := onConvertShelfVisit(arrImageTemp3)

	resp, err := r.dao.CreateVisit(req, bindingData, temp2, temp3, log)
	response.Result = resp
	return
}
