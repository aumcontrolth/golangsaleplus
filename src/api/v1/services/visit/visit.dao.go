package visitService

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"golang-boilerplate-template/src/api/v1/models"
	"time"

	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/database"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/logger"
	"gitlab.com/dplus-dev/zettasoft/backend/microservice/pkg/common-utils/utils"
)

type IServiceDaoVisit interface {
	GetSuggestionVisit(logger.LogModel) (response []BeanGetSuggestionVisit, err error)
	GetBestSellerCustomer(models.GetBestSellerCustomerRequest, logger.LogModel) (response []BeanGetBestSellerCustomer, err error)
	CreateVisit(models.CreateVisitRequest, string, string, string, logger.LogModel) ([]BeanCreateVisit, error)
}

type serviceDaoVisit struct {
	confDB utils.SystemDatabase
}

func NewIServiceDaoVisit(confDB utils.SystemDatabase) IServiceDaoVisit {
	return &serviceDaoVisit{
		confDB: confDB,
	}
}

func (r *serviceDaoVisit) GetSuggestionVisit(log logger.LogModel) (response []BeanGetSuggestionVisit, err error) {
	startStep := time.Now()
	_, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()

	// reqByte, err := json.Marshal(req)
	logStepRequest := logger.LogStepRequest{
		StepName:  "DAO - GetSuggestionVisit",
		StartDate: utils.ConvDatetimeFormatLog(startStep),
		// StepRequest: string(reqByte),
		Endpoint: "ZETTA",
		Method:   "DAO - GetSuggestionVisit",
		System:   "ZETTA",
	}

	client, err := database.GetMsClient(r.confDB.Mysql.Uri)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMsClient"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}

	stmt, err := client.Prepare(`EXEC proc_Visit_getSuggestion`)
	if err != nil {
		logStepRequest.ResultDesc = "Error : Prepare Statement proc_Visit_getSuggestion"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}

	res, err := stmt.Query()
	if err != nil {
		logStepRequest.ResultDesc = "Error : Exec proc_Visit_getSuggestion"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}
	defer stmt.Close()

	for res.Next() {
		var bean BeanGetSuggestionVisit
		errNext := res.Scan(&bean.VisitSuggestId, &bean.VisitSuggestName)
		if errNext != nil {
			logStepRequest.ResultDesc = "Error : Exec proc_Visit_getSuggestion Res Next Failed"
			logStepRequest.Response = errNext.Error()
			logStepRequest.ResultCode = "500"
			logger.LogStep(logStepRequest, log, startStep)
			return
		}
		response = append(response, bean)
	}

	res.Close()
	// fmt.Println(response)
	return
}

func (r *serviceDaoVisit) GetBestSellerCustomer(req models.GetBestSellerCustomerRequest, log logger.LogModel) (response []BeanGetBestSellerCustomer, err error) {
	startStep := time.Now()
	_, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()

	reqByte, err := json.Marshal(req)
	logStepRequest := logger.LogStepRequest{
		StepName:    "DAO - GetBestSellerCustomer",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: string(reqByte),
		Endpoint:    "ZETTA",
		Method:      "DAO - GetBestSellerCustomer",
		System:      "ZETTA",
	}

	client, err := database.GetMsClient(r.confDB.Mysql.Uri)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMsClient"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}

	stmt, err := client.Prepare(`EXEC proc_Visit_getBestSellerByCustomerAvg3Months @customer_code=@p1`)
	if err != nil {
		logStepRequest.ResultDesc = "Error : Prepare Statement proc_Visit_getBestSellerByCustomerAvg3Months"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}

	res, err := stmt.Query(sql.Named("p1", req.CustomerCode))
	if err != nil {
		logStepRequest.ResultDesc = "Error : Exec proc_Visit_getBestSellerByCustomerAvg3Months"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}
	defer stmt.Close()

	for res.Next() {
		var bean BeanGetBestSellerCustomer
		errNext := res.Scan(&bean.ItemCode, &bean.AmountAVG, &bean.ItemName, &bean.Amount, &bean.Status)
		if errNext != nil {
			fmt.Println("###########")
			fmt.Println(errNext)
			logStepRequest.ResultDesc = "Error : Exec proc_Visit_getBestSellerByCustomerAvg3Months Res Next Failed"
			logStepRequest.Response = errNext.Error()
			logStepRequest.ResultCode = "500"
			logger.LogStep(logStepRequest, log, startStep)
			return
		}
		response = append(response, bean)
	}

	res.Close()
	// fmt.Println(response)
	return
}

func (r *serviceDaoVisit) CreateVisit(req models.CreateVisitRequest, bindingData string, temp2 string, temp3 string, log logger.LogModel) (response []BeanCreateVisit, err error) {
	startStep := time.Now()
	_, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()

	reqByte, err := json.Marshal(req)
	logStepRequest := logger.LogStepRequest{
		StepName:    "DAO - CreateVisit",
		StartDate:   utils.ConvDatetimeFormatLog(startStep),
		StepRequest: string(reqByte),
		Endpoint:    "ZETTA",
		Method:      "DAO - CreateVisit",
		System:      "ZETTA",
	}

	client, err := database.GetMsClient(r.confDB.Mysql.Uri)
	if err != nil {
		logStepRequest.ResultDesc = "Error : GetMsClient"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}

	waitSQL := fmt.Sprintf("%s %s EXEC DT23P_Visit_CreateVisit %s, @temp2, @temp3", temp2, temp3, bindingData)

	stmt, err := client.Prepare(waitSQL)
	if err != nil {
		logStepRequest.ResultDesc = "Error : Prepare Statement DT23P_Visit_CreateVisit"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}

	args := make([]interface{}, len(bindingData))
	for i, v := range bindingData {
		args[i] = v
	}
	res, err := stmt.Query(args...)
	if err != nil {
		logStepRequest.ResultDesc = "Error : Exec DT23P_Visit_CreateVisit"
		logStepRequest.Response = err.Error()
		logStepRequest.ResultCode = "500"
		logger.LogStep(logStepRequest, log, startStep)
		return
	}
	defer stmt.Close()

	for res.Next() {
		var bean BeanCreateVisit
		errNext := res.Scan(&bean.Success, &bean.VisitId)
		if errNext != nil {
			fmt.Println("###########")
			fmt.Println(errNext)
			logStepRequest.ResultDesc = "Error : Exec DT23P_Visit_CreateVisit Res Next Failed"
			logStepRequest.Response = errNext.Error()
			logStepRequest.ResultCode = "500"
			logger.LogStep(logStepRequest, log, startStep)
			return
		}
		response = append(response, bean)
	}

	res.Close()
	// fmt.Println(response)
	return

}
