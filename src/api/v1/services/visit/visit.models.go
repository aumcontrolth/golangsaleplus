package visitService

type BeanGetSuggestionVisit struct {
	VisitSuggestId   string `json:"visitSuggestId"`
	VisitSuggestName string `json:"visitSuggestName"`
}

type BeanGetBestSellerCustomer struct {
	ItemCode  *string `json:"itemCode"`
	AmountAVG *uint64 `json:"amountAVG"`
	ItemName  *string `json:"itemName"`
	Amount    *uint64 `json:"amount"`
	Status    *uint8  `json:"status"`
}

type BeanCreateVisit struct {
	Success *string `json:"success"`
	VisitId *string `json:"visitId"`
}
